# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

Please modify this file to include a description of your project.
You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.\
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you